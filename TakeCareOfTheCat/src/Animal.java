enum State {DEAD,ALIVE}


public abstract class Animal {
	String animalname;
	int satiety = 30;
	State state = State.ALIVE;
	void eat(Feed feed)
	{
		int wanteatamont = 100 - this.satiety;	//食べたい量
		if(feed.amount < wanteatamont)
		{
			this.satiety += feed.amount;
		}
		else
		{
			this.satiety = 100;
		}
		feed.beeaten(wanteatamont);
	}
	
	State StateCheck()
	{
		if(this.satiety < 0){
			return State.DEAD;
		}
		return State.ALIVE;
	}
	
	Animal(String animalname)
	{
		this.animalname = animalname;
	}
	Animal()
	{
		this.animalname = "不明";
		this.satiety = 30;
		this.state = State.ALIVE;
	}
}
