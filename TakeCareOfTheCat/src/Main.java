

public class Main {
	public static void main(String[] args)
	{
		Cat cat = new Cat();
		Fish fish = new Fish();
		Meat meat = new Meat();
		Stone stone = new Stone();
		
		cat.eat(fish);
		cat.eat(stone);
		cat.eat(meat);
	}
}
