
public class BadStomachState implements StomachState{
	public StomachState eat(Putable putable){
		this.talk();
		return vomit();
	}
	
	public void talk(){
		System.out.println("オエー");
	}
	
	private StomachState vomit(){
		return new GoodStomachState();
	}
}
