
public class GoodStomachState implements StomachState{
	public StomachState eat(Putable putable){
		StomachState state = new GoodStomachState();
		if(putable instanceof Stone){
			state = pain();
		}
		this.talk();
		return state;
	}
	
	public void talk(){
		System.out.println("ごちそうさま");
	}
	
	private StomachState pain(){
		return new BadStomachState();
	}
}
