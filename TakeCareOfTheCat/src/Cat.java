
public class Cat {
	private StomachState stomach = null;
	
	Cat(){
		this.stomach = new GoodStomachState();
	}
	
	public void eat(Putable putable){
		this.stomach = this.stomach.eat(putable);
	}
	
	public void talk(){
		this.stomach.talk();
	}
}
