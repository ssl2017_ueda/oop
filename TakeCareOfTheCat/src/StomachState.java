
public interface StomachState {
	public StomachState eat(Putable putable);
	public void talk();
}
